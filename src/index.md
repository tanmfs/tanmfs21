---
layout: layouts/two-col.njk
title: There are no more four seasons
blocks:
  right:
  - blocks/reviews.njk
eleventyNavigation:
  key: Home
  order: "1"
  icon: home

---
![There are no more four seasons](/assets/img/wrango.jpg "There are no more four seasons")

**Two CDs out:** [**buy and read more here!**](https://nm4s.bandcamp.com/album/there-are-no-more-four-seasons)

The followup to the legendary self-titled debut CD by there are no more four seasons
Released by **SEKT records**

***

## Why??

Why do we do stuff like recompose Vivaldi's four seasons for violin and live electronics? Well...

Vivaldi’s _Four Seasons_ consists of 12 great songs. But one year in Italy in 1725 doesn’t seem to have much to do with the way we live today, except maybe on a psychological level. And the psychological level has a tendency to disappear when so much of our experience of the work has to do with Baroque practice, concert halls, elevator music, and the cultural sheen in which violin concertos usually exist. The same goes for most pieces in the Classical repertoire.

But the pieces are still out there, and we like some of them a lot. We’d play the music in its original form if it was worth doing. But we only play new music. So we decided to turn these things back into new music. Not to make it hip, just to make it worth playing, and listening to, again.

We do stuff like switch around the order of the seasons too, because you can’t really say that there are any seasons anymore, at least not chronologically. We travel, watch color TV, complain about global warming, have air conditioning and central heating. And a lot of our feeling for the seasons has to do with memories: what we did last summer, the skiing vacation in 1999, that trip to the West Indies. So what we do could end up being an entire year in your head—or it might just take an hour. It is up to you.
