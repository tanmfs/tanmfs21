---
layout: layouts/photo.njk
title: There are no more seasons 2
alt: There are no more seasons 2
copyright: Marcus Wrangö
src: '/assets/img/2_large.jpg'
thumb: '/assets/img/2.jpg'
tif: '/assets/img/2_large.tif'
---

![There are no more seasons 2](/assets/img/2_large.jpg)
