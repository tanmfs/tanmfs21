---
title: The best classical album of 2013
author: Martin Nyström
date: 2020-12-24
magazine: Dagens Nyheter
excerpt: The best classical album of 2013.
excerpt_se: Det bästa klassiska albumet 2013.
layout: layouts/review.njk
permalink: reviews/{{ title | slug }}/index.html

---
The best classical album of 2013 according to **Martin Nyström** *(Dagens Nyheter)*