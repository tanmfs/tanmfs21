---
layout: redirect.njk
title: Concerts
templateClass: page
redirect: { to: '/' }
#eleventyNavigation:
#  key: Concerts
#  order: "5"
#  icon: fire
---

We have toured around the globe with our duo and here are a selection of the concerts we have had during the years.

## 2021

* **GAS-festival 2021** (ReBiber)
* **Stockholm New Music 2021** (ReBergström)

## 2020

* ...
* ...

## 2019

* ...
* ...

## 2018

* ...
* ...

## 2017

* ...
* ...

## 2016

* ...
* ...

## 2015

* ...
* ...

## 2014

* ...
* ...

## 2013

* ...
* ...

## 2012

* ...
* ...
