---
layout: layouts/two-col.njk
title: There are no more four seasons
blocks:
  right:
  - blocks/recensioner.njk
templateClass: page
eleventyNavigation:
  key: In Swedish
  order: "8"
  icon: flag

---
![image of duo](/assets/img/wrango.jpg)

## Varför?

Årstiderna består av 12 fantastiska låtar. Men liknar vår värld den som Vivaldi skrev om? Ett år i Italien 1725 har kanske inte så mycket att göra med det vi upplever och gör idag. Förutom på de psykologiska planet då. Och det psykologiska planet försvinner lätt när så mycket av vår upplevelse av verket handlar om barockpraxis, orkestersalar och den finkulturella skimmret i vilket violinkonserter brukar vistas.

Men verket kvarstår och vi tycker väldigt mycket om det. Vi hade hemskt gärna spelat det för er om det hade varit värt det. Men vi spelar bara nutida musik. Så vi bestämde oss för att göra det som nutida musik. Inte för att hotta upp det, utan för att vi ska känna att det var tillräckligt värt att spela för er. Och för att komma ihåg att barockmusik var den tidens nutida musik.

Dessutom kan man inte riktigt säga att det finns några årstider längre: vi reser, ser på TV, klagar över global värmning, har AC och centralvärme. Och mycket av vår upplevelse av årstiderna har med minnen att göra, vad vi gjorde förra sommaren, skidsemestern 1999. Så detta blir kanske ett år i ditt huvud—eller också tar det bara en timme.

Vi har stuvat om lite i satsordningen också, mest för att förstärka känslan av att vi upplever årstider på ett annat sätt än tidigare. Det gör inte så mycket—det är faktiskt ganska få som vet exakt vilken årstid en viss sats ur Årstiderna är hämtad ifrån, man konstaterar bara att de



## Vilka är vi?


![Mattias Petersson](/assets/img/matt.jpg){.w-full}<small>Photo: Marcus Wrangö</small>

**Mattias Petersson** föddes 1972 på Öland. Han började sin musikerbana som pianist men nu arbetar han som tonsättare samt spelar elektroniska instrument. Han jobbar mest inom experimentell electroakustisk musik och ljudkonst, men han har också arbetat i mera poporienterade projekt som arrangör, tonsättare, producent och musiker.

![George Kentros](/assets/img/george.jpg){.w-full}<small>Photo: Anonymous</small>

Violinisten **George Kentros** föddes i USA och utbildades på Yale University och Mannes School of Music samt på Musikhögskolan i Stockholm. Han har på senare år specialiserat sig på den allra nyaste konstmusiken, främst med kvartetten "pärlor för svin", som han grundade 1995. Hittils har han hunnit beställa och uruppföra fler än 100 verk av tonsättare från 19 länder, och framträtt som solist och kammarmusiker runtom i Europa samt i USA, Japan och Centralamerika.